from django.test import TestCase,Client
from django.urls import resolve
from .views import home,SignUp

# Create your tests here.
class LogTest(TestCase):
    def test_log_url_is_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code,200)

    def test_log_using_home_func(self):
        found = resolve('/')
        self.assertEqual(found.func, home)

    def test_view_using_correct_template(self):
        response = Client().get('/')
        self.assertTemplateUsed(response,'home.html')