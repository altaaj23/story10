from django.urls import path, include
from .views import SignUp,home
from django.views.generic.base import TemplateView

urlpatterns = [
    path('', include('django.contrib.auth.urls')),
    path('', home, name='home'),
    path('signup/', SignUp.as_view(), name='signup'),

]